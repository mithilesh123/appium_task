
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
//import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import io.appium.java_client.remote.MobileCapabilityType;


public class nativeAppCall {
WebDriver driver;

@Test
public void setUp() throws MalformedURLException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
	DesiredCapabilities capabilities = DesiredCapabilities.android();

	// set the capability to execute test in chrome browser
	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");

	// set the capability to execute our test in Android Platform
	capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);

	// we need to define platform name
	capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

	// Set the device name as well (you can give any name)
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

	//  set the android version as well 
	capabilities.setCapability(MobileCapabilityType.VERSION, "8.1.0");
 
   
   capabilities.setCapability("appPackage", "com.android.server.telecom");
// This package name of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity","ccom.android.server.telecom.components.UserCallActivity"); // This is Launcher activity of your app (you can get it from apk info app)
//Create RemoteWebDriver instance and connect to the Appium server
 //It will launch the Calculator App in Android Device using the configurations specified in Desired Capabilities
   driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
     
   driver.findElement(By.className("android.widget.ImageButton"));
   driver.findElement(By.xpath("[@text='2']"));
   driver.findElement(By.xpath("[@resource-id='com.samsung.android.contacts:id/dialButtonImage']"));
   
   
  
  

	//close the app
	driver.quit();

}

}