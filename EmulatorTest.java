
import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;

public class EmulatorTest {
	@Test
	public void test1() throws MalformedURLException, InterruptedException {

		DesiredCapabilities capabilities = DesiredCapabilities.android();

		// set the capability to execute test in chrome browser
		capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, BrowserType.CHROME);

		// set the capability to execute our test in Android Platform
		capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);

		// we need to define platform name
		capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

		// Set the device name as well (you can give any name)
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

		//  set the android version as well 
		capabilities.setCapability(MobileCapabilityType.VERSION, "8.1.0");

		// Create object of URL class and specify the appium server address
		URL url = new URL("http://127.0.0.1:4723/wd/hub");

		// Create object of  AndroidDriver class and pass the url and capability
		// that we created
		WebDriver driver = new AndroidDriver(url, capabilities);

		// Open url
		driver.get("http://www.facebook.com");

		// print the title
		System.out.println("Title " + driver.getTitle());
        
		Thread.sleep(5000);
		
		// enter username
		driver.findElement(By.name("email")).sendKeys("writecode8@gmail.com");

		// enter password
		driver.findElement(By.name("pass")).sendKeys("Mangoes@1");

		// click on submit button
		driver.findElement(By.id("u_0_5")).click();
		
		Thread.sleep(10000);
		
		//Enter value in search
		driver.findElement(By.xpath("//input[@class='_5whq input']")).sendKeys("Krishna Singh");
		
		
		//click on search button
		driver.findElement(By.xpath("//button[@name='submit']")).click();
		
		
		Thread.sleep(3000);
		
		driver.findElement(By.xpath("//*[@id=\"friends_center_search_list\"]/div[1]/div[2]/div/h3/a")).click();

		Thread.sleep(10000);
		// close the browser
		//driver.close();
	}
}
