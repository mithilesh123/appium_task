
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
//import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import io.appium.java_client.remote.MobileCapabilityType;


public class TestNativeAppFlipkart {
WebDriver driver;

@Test
public void setUp() throws MalformedURLException, InterruptedException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
DesiredCapabilities capabilities = DesiredCapabilities.android();
	
	//driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

	// set the capability to execute test in chrome browser
	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");

	// set the capability to execute our test in Android Platform
	capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
	
	capabilities.setCapability("platformName", "Android");
	capabilities.setCapability("name", "Google_Pixel___7_1_0___API_25___1080x1920");
	

	// we need to define platform name
	capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

	// Set the device name as well (you can give any name)
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

	//  set the android version as well 
	capabilities.setCapability(MobileCapabilityType.VERSION, "7.1.0");
 
   
   capabilities.setCapability("appPackage", "com.flipkart.android");
// This package name of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity","com.flipkart.android.SplashActivity"); // This is Launcher activity of your app (you can get it from apk info app)
//Create RemoteWebDriver instance and connect to the Appium server
 
  driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
  driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
   Thread.sleep(10000);
   driver.findElement(By.xpath("//android.widget.Button[@resource-id='com.flipkart.android:id/btn_mlogin']")).click();
   
   //signin
   //email
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys("mithi.rocks123@gmail.com");
   
   //password
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).sendKeys("Mangoes@1");
   
   driver.findElement(By.xpath("//android.widget.Button[@text='SIGN IN']")).click();
   
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   
   
   //click on my account
   driver.findElement(By.xpath("//android.widget.TextView[@text='My Account']")).click();
   
   driver.findElement(By.xpath("//android.view.View[@index='19']")).click();
   
   driver.findElement(By.xpath("//android.view.View[@index='0']")).click();
   
   //Address Fields
   driver.findElement(By.xpath("//android.widget.EditText[@index='2']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@index='2']")).sendKeys("Bangaluru");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='addressLine2']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='addressLine2']")).sendKeys("Electronic City,Neeladri");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='addressLine1']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='addressLine1']")).sendKeys("10th Cross");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='pincode']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='pincode']")).sendKeys("560100");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='state']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='state']")).sendKeys("Karnataka");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='name']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='name']")).sendKeys("Appium Automation");
   
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='phone']")).click();
   driver.findElement(By.xpath("//android.widget.EditText[@resouce-id='phone']")).sendKeys("7896541236");
   
   driver.findElement(By.xpath("//android.view.View[@index='0']")).click();
   
   driver.findElement(By.xpath("//android.widget.Button[@index='0']")).click();
   
   
 
   
   //driver.findElement(By.xpath("//android.widget.TextView[@text='shoes' and resource-id='com.flipkart.android:id/txt_title']")).click();
   Thread.sleep(10000);
	//close the app
	driver.quit();

}

}