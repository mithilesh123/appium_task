
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import io.appium.java_client.android.AndroidKeyCode;
import io.appium.java_client.remote.MobileCapabilityType;


public class FlipkartTest3 {
WebDriver driver;

@Test
public void setUp() throws MalformedURLException, InterruptedException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
DesiredCapabilities capabilities = DesiredCapabilities.android();
	
	//driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

	// set the capability to execute test in chrome browser
	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");

	// set the capability to execute our test in Android Platform
	capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
	
	capabilities.setCapability("platformName", "Android");
	capabilities.setCapability("name", "Google_Pixel___7_1_0___API_25___1080x1920");
	

	// we need to define platform name
	capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

	// Set the device name as well (you can give any name)
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

	//  set the android version as well 
	capabilities.setCapability(MobileCapabilityType.VERSION, "7.1.0");
 
   
   capabilities.setCapability("appPackage", "com.flipkart.android");
// This package name of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity","com.flipkart.android.SplashActivity"); // This is Launcher activity of your app (you can get it from apk info app)
//Create RemoteWebDriver instance and connect to the Appium server
 
   driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
   driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
   Thread.sleep(10000);
   driver.findElement(By.xpath("//android.widget.Button[@resource-id='com.flipkart.android:id/btn_mlogin']")).click();
   
   //signin
   //email
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys("mithi.rocks123@gmail.com");
   
   //password
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).sendKeys("Mangoes@1");
   
   driver.findElement(By.xpath("//android.widget.Button[@text='SIGN IN']")).click();
   
   //Select a product eg. Nexus 6P
   driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).sendKeys("Nexus 6P");
   driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.TextView[@text='Nexus 6P Special Edition (Gold, 64 GB)']")).click();
   
   //save raing and price
   String ratingNexus = driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).getText();
   String priceNexusText = driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).getText();
   String priceNexus = priceNexusText.substring(1, priceNexusText.length()-1);
   
   //click on heart icon
   driver.findElement(By.xpath("//android.view.View[@index='1']")).click();
   
   //click on back button to return to home page
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.ImageView[@index='0']")).click();
   
   
   //SELECTING 2nd Product
   //select samsung galaxy A9
   driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).sendKeys("Samsung Galaxy A9");
   driver.findElement(By.xpath("//android.widget.RelativeLayout[@index='0']")).click();
   
   //click on samsung galaxy A9
   driver.findElement(By.xpath("//android.widget.TextView[@text='Samsung Galaxy A9 (Lemonade Blue, 128 GB)']")).click();
   
   //save rating and price
   String ratingGalaxy = driver.findElement(By.xpath("//android.widget.TextView[@text=' 4.3 ★']")).getText();
   String priceGalaxyText = driver.findElement(By.xpath("//android.widget.TextView[@text='₹39,990']")).getText();
   String priceGalaxy = priceGalaxyText.substring(1, priceGalaxyText.length()-1);
   
   //click on heart icon
   driver.findElement(By.xpath("//android.view.View[@index='1']")).click();
   
  //click on back button to return to home page
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.ImageView[@index='0']")).click();
   
   //click on app drawer and wishlist
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   driver.findElement(By.xpath("//android.widget.TextView[@text='My Wishlist']")).click();
   
   //Nexus 6P save rating and price
   List<WebElement> wishRatingNexusList= driver.findElements(By.xpath("//android.widget.TextView[@resource-id='com.flipkart.android:id/product_list_product_item_rating']"));
   String wishRatingNexus = wishRatingNexusList.get(0).getText();
   List<WebElement> wishPriceNexusTextList = driver.findElements(By.xpath("//android.widget.TextView[@resource-id='com.flipkart.android:id/product_list_product_item_price']"));
   String wishPriceNexusText = wishPriceNexusTextList.get(0).getText();
   String wishPriceNexus = wishPriceNexusText.substring(1, wishPriceNexusText.length()-1);
   
   
   //Samsung galaxy save rating and price
 //Nexus 6P save rating and price
   List<WebElement> wishRatingSamsungList = driver.findElements(By.xpath("//android.widget.TextView[@resource-id='com.flipkart.android:id/product_list_product_item_rating']"));
   String wishRatingSamsung = wishRatingSamsungList.get(1).getText();
   List<WebElement> wishPriceSamsungTextList = driver.findElements(By.xpath("//android.widget.TextView[@resource-id='com.flipkart.android:id/product_list_product_item_price']"));
   String wishPriceSamsungText = wishPriceSamsungTextList.get(1).getText();
   String wishPriceSamsung = wishPriceNexusText.substring(1, wishPriceNexusText.length()-1);
   
   
   //CHECKING FOR MATCH OF RATING AND PRICE
   
   if(ratingNexus.equalsIgnoreCase(wishRatingNexus)) {
	   System.out.println("RATINGS OF NEXUS 6P MATCH FOR SAVED AND WISHLIST");
   }
   else {
	   System.out.println("RATINGS OF NEXUS 6P DO NOT MATCH FOR SAVED AND WISHLIST");
   }
   
   if(priceNexus.equalsIgnoreCase(wishPriceNexus)) {
	   System.out.println("PRICE OF NEXUS 6P MATCH FOR SAVED AND WISHLIST");
   }
   else {
	   System.out.println("PRICE OF NEXUS 6P DO NOT MATCH FOR SAVED AND WISHLIST");
   }
   
   if(ratingGalaxy.equalsIgnoreCase(wishRatingSamsung)) {
	   System.out.println("RATING OF SAMSUNG GALAXY A9 MATCH FOR SAVED AND WISHLIST");
   }
   else 
   {
	   System.out.println("RATING OF SAMSUNG GALAXY A9 DO NOT MATCH FOR SAVED AND WISHLIST");
   }
   
   if(priceGalaxy.equalsIgnoreCase(wishPriceSamsung)) {
	   System.out.println("PRICE OF SAMSUNG GALAXY A9 MATCH FOR SAVED AND WISHLIST");
   }
   else 
   {
	   System.out.println("PRICE OF SAMSUNG GALAXY A9 DO NOT MATCH FOR SAVED AND WISHLIST");
   }
   
   Thread.sleep(10000);
	//close the app
	driver.quit();

}

}