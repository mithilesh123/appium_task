
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import io.appium.java_client.remote.MobileCapabilityType;


public class FlipkartTest2 {
WebDriver driver;

@Test
public void setUp() throws MalformedURLException, InterruptedException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
DesiredCapabilities capabilities = DesiredCapabilities.android();
	
	//driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

	// set the capability to execute test in chrome browser
	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");

	// set the capability to execute our test in Android Platform
	capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
	
	capabilities.setCapability("platformName", "Android");
	capabilities.setCapability("name", "Google_Pixel___7_1_0___API_25___1080x1920");
	

	// we need to define platform name
	capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

	// Set the device name as well (you can give any name)
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

	//  set the android version as well 
	capabilities.setCapability(MobileCapabilityType.VERSION, "7.1.0");
 
   
   capabilities.setCapability("appPackage", "com.flipkart.android");
// This package name of your app (you can get it from apk info app)
	capabilities.setCapability("appActivity","com.flipkart.android.SplashActivity"); // This is Launcher activity of your app (you can get it from apk info app)
//Create RemoteWebDriver instance and connect to the Appium server
 
   driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
   driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
   Thread.sleep(10000);
   driver.findElement(By.xpath("//android.widget.Button[@resource-id='com.flipkart.android:id/btn_mlogin']")).click();
   
   //signin
   //email
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@index='0']")).sendKeys("mithi.rocks123@gmail.com");
   
   //password
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).clear();
   driver.findElement(By.xpath("//android.widget.EditText[@resource-id='com.flipkart.android:id/et_password']")).sendKeys("Mangoes@1");
   
   driver.findElement(By.xpath("//android.widget.Button[@text='SIGN IN']")).click();
   
   //Click on App Drawer
   driver.findElement(By.xpath("//android.widget.ImageButton[@index='0']")).click();
   
   //click on Fashion
   driver.findElement(By.xpath("//android.widget.TextView[@index='2']")).click();
   
   //click on women
   driver.findElement(By.xpath("//android.widget.TextView[@text='Women']")).click();
   
   
   //click on clothing
   driver.findElement(By.xpath("//android.widget.TextView[@text='Clothing']")).click();
   
   //click on ethic wear
   driver.findElement(By.xpath("//android.widget.TextView[@index='0']")).click();
   
   //click on sarees
   driver.findElement(By.xpath("//android.widget.TextView[@index='1']")).click();
   
   
   //click on banner image
   driver.findElement(By.xpath("//android.widget.ImageView[@index='0']")).click();
   
   //select a saree and color
   driver.findElement(By.xpath("//android.widget.TextView[@index='3']")).click();
   
   //click on buy now 
   driver.findElement(By.xpath("//android.widget.TextView[@text='BUY NOW']")).click();
   
   //verify whether continue button is present
   String cont = driver.findElement(By.xpath("//android.view.View[@index='0]")).getText();
   
   if(cont.equalsIgnoreCase("CONTINUE")) {
	   System.out.println("CONTINUE Button is present");
   }
   else {
	   System.out.println("CONTINUE Button is not present");
   }
   
   Thread.sleep(10000);
	//close the app
	driver.quit();

}

}