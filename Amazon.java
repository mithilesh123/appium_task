
 
import io.appium.java_client.android.AndroidDriver;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;


import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.BrowserType;
//import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.*;

import io.appium.java_client.remote.MobileCapabilityType;


public class Amazon {
WebDriver driver;

@Test
public void setUp() throws MalformedURLException, InterruptedException{
	//Set up desired capabilities and pass the Android app-activity and app-package to Appium
	DesiredCapabilities capabilities = DesiredCapabilities.android();
	
	//driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

	// set the capability to execute test in chrome browser
	capabilities.setCapability(MobileCapabilityType.BROWSER_NAME, "");

	// set the capability to execute our test in Android Platform
	capabilities.setCapability(MobileCapabilityType.PLATFORM, Platform.ANDROID);
	
	capabilities.setCapability("platformName", "Android");
	

	// we need to define platform name
	capabilities.setCapability(MobileCapabilityType.PLATFORM, "Android");

	// Set the device name as well (you can give any name)
	capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "my phone");

	//  set the android version as well 
	capabilities.setCapability(MobileCapabilityType.VERSION, "8.1.0");
 
   

	capabilities.setCapability("appPackage", "in.amazon.mShop.android.shopping");
	capabilities.setCapability("appActivity", "com.amazon.mShop.home.HomeActivity");
    driver = new RemoteWebDriver(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);
   
 
		
			driver.manage().timeouts().implicitlyWait(80, TimeUnit.SECONDS);
			Thread.sleep(10000);
			
			driver.findElement(By.xpath("//android.widget.EditText[@text='Search']")).click();
			driver.findElement(By.xpath("//android.widget.EditText[@text='Search']")).sendKeys("Shoes");
			//driver.findElement(By.xpath("//android.widget.EditText[@text='Search']")).sendKeys(Keys.RETURN);
			Thread.sleep(10000);
			driver.findElement(By.xpath("//android.widget.ImageView[@resource-id='in.amazon.mShop.android.shopping:id/rs_results_image']")).click();
			Thread.sleep(10000);
			driver.quit();
 
	}
 
}